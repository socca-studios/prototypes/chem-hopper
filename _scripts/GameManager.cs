using System.Collections.Generic;
using ChemHopper._scripts.Models;
using Godot;

namespace ChemHopper._scripts; 

public partial class GameManager : Node {
  public static List<Player> Players = new();

  public static bool IsPlayerTagFree(string tag) {
    foreach (Player player in Players) {
      if (player.Name == tag) return false;
    }

    return true;
  }

  public static string ListPlayers() {
    string tags = "";
    foreach (Player player in Players) {
      tags += $"{player.Name} ({player.Id}), ";
    }

    return tags.Substring(0, tags.Length - 2);
  }
  
  public static string ListPlayerTags() {
    string tags = "";
    foreach (Player player in Players) {
      tags += $"{player.Name}, ";
    }

    return tags.Substring(0, tags.Length - 2);
  }
}