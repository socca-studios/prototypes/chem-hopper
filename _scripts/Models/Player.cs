namespace ChemHopper._scripts.Models; 

public class Player {
  public int Id;
  public string Name;
  public int Score;
}