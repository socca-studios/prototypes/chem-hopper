using System;
using System.Diagnostics;
using Godot;

namespace ChemHopper._scripts; 

public partial class BeingController : CharacterBody3D {
	public event Action PlayerStabbed;
	public event Action PlayerParried;
	
	[Export] private float _rotationSpeed = 5f;
	[Export] private float _speed = 5f;
	[Export] private float _sprintModifier = 1.75f;

	public Label3D Label;
	
	private LevelController _level;
	private MultiplayerSynchronizer _syncNode;
	
	public override void _Ready() {
		_level = GetNode<LevelController>("/root/Level");
		_syncNode = GetNode<MultiplayerSynchronizer>("MultiplayerSynchronizer");
		_syncNode.SetMultiplayerAuthority(int.Parse(Name));
		
		Label = GetNode<Label3D>("Label");
	}
	
	public override void _PhysicsProcess(double delta) {
		if (_syncNode.GetMultiplayerAuthority() != Multiplayer.GetUniqueId()) return;
		
		float speed = Input.IsActionPressed("sprint") ? _speed * _sprintModifier : _speed;
		
		HandleMovement(speed, delta);
		// HandleAction();
	}

	private void HandleMovement(float speed, double delta) {
		Vector3 rotation = Rotation;
		Vector3 velocity = Velocity;
		
		// Handling gravity
		if (!IsOnFloor()) {
			velocity.Y -= _level.Gravity * (float)delta;
		}
		
		// Handling movement
		Vector2 input = Input.GetVector("move-left", "move-right", "move-forward", "move-backward").Normalized();
		Vector3 direction = Transform.Basis * new Vector3(input.X, 0f, input.Y).Normalized();

		if (IsOnFloor()) {
			if (direction != Vector3.Zero) {  // Player moves
				velocity.X = direction.X * speed;
				velocity.Z = direction.Z * speed;
			} else {  // Player stopped moving
				velocity.X = Mathf.Lerp(velocity.X, direction.X * speed, (float)delta * 5f);
				velocity.Z = Mathf.Lerp(velocity.Z, direction.Z * speed, (float)delta * 5f);
			}
		} else {  // Player is in the air
			velocity.X = Mathf.Lerp(velocity.X, direction.X * speed, (float)delta * 2.5f);
			velocity.Z = Mathf.Lerp(velocity.Z, direction.Z * speed, (float)delta * 2.5f);
		}

		// Handling jumps
		if (Input.IsActionJustPressed("jump") && IsOnFloor()) {
			velocity.Y = 4.5f;
		}
		
		// Handling rotation
		float inputRotation = Input.GetAxis("rotate-right", "rotate-left");
		rotation.Y += inputRotation * (float)delta * _rotationSpeed;
		
		Rotation = rotation;
		Velocity = velocity;
		
		MoveAndSlide();
	}

	private void HandleAction() {
		if (Input.IsActionJustPressed("stab")) {
			PlayerStabbed?.Invoke();
		} else if (Input.IsActionJustPressed("parry")) {
			PlayerParried?.Invoke();
		}
	}
}