using Godot;

namespace ChemHopper._scripts; 

public partial class BulletController : Area3D {
  [Signal] public delegate void HitSomethingEventHandler();
  
  public float Speed { get; set; } = 1f;
  
  public override void _Ready() {
    AreaEntered += CheckForImpact;
  }

  public override void _Process(double delta) {
    Translate(Vector3.Left.Rotated(Rotation, 0) * Speed);
  }

  private void CheckForImpact(Area3D impactedArea) {
    GD.Print($"Impacted {impactedArea}");
    
    impactedArea.QueueFree();
  }
  
}