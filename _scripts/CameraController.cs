using Godot;

namespace ChemHopper._scripts; 

public partial class CameraController : Node3D {
	// Properties
	[Export] private float _moveSpeed = 10f;
	[Export] private float _rotationSpeed = 20f;
	[Export] private float _zoomSpeed = 2.5f;
	[Export] private float _marginToMove = 45;
	[Export] private float _maxFov = 90;
	[Export] private float _minFov = 60;
	
	// Attributes
	private Camera3D _camera;
	private float _baseSpeed;
	private float _moveSprintModifier = 3;
	
	// Initialising & Update
	// ----------------------------------------
	public override void _Ready() {
		_baseSpeed = _moveSpeed;
		_camera = GetNode<Camera3D>("MainCamera");
	}

	public override void _Process(double delta) {
		HandleRotation(delta);
		HandleTranslation(delta);
		HandleZoom(delta);
	}

	// Methods
	// ----------------------------------------
	private void HandleRotation(double delta) {
		float inputRotation = Input.GetAxis("camera-rotate-right", "camera-rotate-left");
		this.Rotation += new Vector3(0f, inputRotation * 0.1f, 0f) * _rotationSpeed * (float)delta;
	}

	private void HandleTranslation(double delta) {
		// Allow speedy movement with "Shift" pressed
		_moveSpeed = Input.IsActionPressed("camera-sprint") ? _baseSpeed * _moveSprintModifier : _baseSpeed;
		
		// Keyboard-based translation
		Vector2 inputDirection = Input.GetVector("camera-left", "camera-right", "camera-front", "camera-back");
		
		TranslateObjectLocal(new Vector3(inputDirection.X, 0f, inputDirection.Y) * _moveSpeed * (float)delta);
		
		// Mouse-based translation
		// Vector2 mousePosition = GetViewport().GetMousePosition();
		// Vector2 windowSize = GetViewport().GetVisibleRect().Size;
		
		// if (mousePosition.X > 0 && mousePosition.X < _marginToMove) { TranslateObjectLocal(Vector3.Left * _moveSpeed * (float)delta); }
		// if (mousePosition.Y > 0 && mousePosition.Y < _marginToMove) { TranslateObjectLocal(Vector3.Forward * _moveSpeed * (float)delta); }
		// if (mousePosition.X < windowSize.X && mousePosition.X > windowSize.X - _marginToMove) { TranslateObjectLocal(Vector3.Right * _moveSpeed * (float)delta); }
		// if (mousePosition.Y < windowSize.Y && mousePosition.Y > windowSize.Y - _marginToMove ) { TranslateObjectLocal(Vector3.Back * _moveSpeed * (float)delta); }
	}
	
	private void HandleZoom(double delta) {
		if (Input.IsActionJustReleased("wheel-up") && _camera.Fov > _minFov) {
			// Zoom
			this.Position += new Vector3(0f, -0.5f, 0f);
			_camera.Fov -= _zoomSpeed;
			_camera.Rotation += new Vector3(0.05f, 0f, 0f);
		} else if (Input.IsActionJustReleased("wheel-down") && _camera.Fov < _maxFov) {
			// Dezoom
			this.Position += new Vector3(0f, 0.5f, 0f);
			_camera.Fov += _zoomSpeed;
			_camera.Rotation += new Vector3(-0.05f, 0f, 0f);
		}
	}
}