using ChemHopper._scripts.Models;
using Godot;

namespace ChemHopper._scripts; 

public partial class MultiplayerController : Control {
  [Export] private string _address = "127.0.0.1";
  [Export] private int _port = 4506;
  [Export] private ENetConnection.CompressionMode _compressionMode = ENetConnection.CompressionMode.RangeCoder;

  private ENetMultiplayerPeer _peer;
  private PackedScene _levelScene = ResourceLoader.Load<PackedScene>("res://scenes/level.tscn");
  private PackedScene _playerScene = ResourceLoader.Load<PackedScene>("res://scenes/being.tscn");

  // UI Nodes
  private Button _hostBtn;
  private Button _joinBtn;
  private Button _playBtn;

  private LineEdit _playerTag;
	
  public override void _Ready() {
    Multiplayer.PeerConnected += OnConnectedPeer;
    Multiplayer.ConnectedToServer += OnConnectedToServer;
    Multiplayer.PeerConnected += OnDisconnectedPeer;
    Multiplayer.ConnectionFailed += OnFailedConnection;
		
    _hostBtn = (Button)FindChild("*Host");
    _hostBtn.Pressed += OnPressedHostBtn;
    _joinBtn = (Button)FindChild("*Join");
    _joinBtn.Pressed += OnPressedJoinBtn;
    _playBtn = (Button)FindChild("*Start");
    _playBtn.Pressed += OnPressedPlayBtn;
    _playerTag = (LineEdit)FindChild("PlayerTagEdit");
  }

  /// <summary>
  /// Runs when a player connects, runs on all peers
  /// </summary>
  /// <param name="id">id of the connected player</param>
  /// <exception cref="NotImplementedException"></exception>
  private void OnConnectedPeer(long id) {
    GD.Print($"A new player connected! ({id})");
    Rpc(nameof(SendPlayerInfo), _playerTag.Text, Multiplayer.GetUniqueId());
  }
  
  /// <summary>
  /// Runs when the connection is successful, only runs on the client
  /// </summary>
  /// <exception cref="NotImplementedException"></exception>
  private void OnConnectedToServer() {
    // GD.Print("Connected to the server!");
  }

  /// <summary>
  /// Runs when a player connects, runs on all peers
  /// </summary>
  /// <param name="id">id of the disconnected player</param>
  /// <exception cref="NotImplementedException"></exception>
  private void OnDisconnectedPeer(long id) {
    GD.Print($"A player disconnected! ({id})");
  }
  
  /// <summary>
  /// Runs when the connection fails, only runs on the client
  /// </summary>
  /// <exception cref="NotImplementedException"></exception>
  private void OnFailedConnection() {
    GD.Print("Connection failed!");
  }
  
  private void OnPressedHostBtn() {
    _peer = new ENetMultiplayerPeer();
    var error = _peer.CreateServer(_port, 8);

    // Handles hosting errors
    if (error != Error.Ok) {
      GD.Print($"Error, cannot host! ({error}");
      return;
    }
    
    _peer.Host.Compress(_compressionMode);
    Multiplayer.MultiplayerPeer = _peer;
    GD.Print("Waiting for players...");
    SendPlayerInfo(_playerTag.Text, 1);
  }

  private void OnPressedJoinBtn() {
    _peer = new ENetMultiplayerPeer();
    var error = _peer.CreateClient(_address, _port);
    
    _peer.Host.Compress(_compressionMode);
    Multiplayer.MultiplayerPeer = _peer;
    GD.Print("Connecting to server...");
  }

  private void OnPressedPlayBtn() {
    Rpc("StartGame");
  }

  [Rpc(MultiplayerApi.RpcMode.AnyPeer, CallLocal = true)]
  private void StartGame() {
    GD.Print("Launching the game!");
    GD.Print($"Currently connected: {GameManager.ListPlayers()}");
    
    LevelController level = _levelScene.Instantiate<LevelController>();
    GetTree().Root.AddChild(level);
    this.Hide();
  }

  [Rpc(MultiplayerApi.RpcMode.AnyPeer)]
  private void SendPlayerInfo(string name, int id) {
    if (name == "") name = id.ToString();
    Player player = new Player() { Name = name, Id = id };
    
    if (GameManager.IsPlayerTagFree(name)) {
      GameManager.Players.Add(player);
    } else {
      GD.Print($"Tag \"{name}\" is already taken!");
    }

    if (Multiplayer.IsServer()) {
      foreach (Player p in GameManager.Players) {
        Rpc("SendPlayerInfo", p.Name, p.Id);
      }
    }
  }
}