using ChemHopper._scripts.Models;
using Godot;

namespace ChemHopper._scripts; 

public partial class LevelController : Node3D {
	[Export] private float _bulletSpeed = 20f;
	
	public float Gravity { get; private set; }
	
	private GameManager _game;
	private PackedScene _playerScene = ResourceLoader.Load<PackedScene>("res://scenes/being.tscn");
	
	// private BeingController Being { get; set; }
	
	public override void _Ready() {
		// Initialising and Settings
		Gravity = (float)ProjectSettings.GetSetting("physics/3d/default_gravity");
		_game = GetNode<GameManager>("/root/GameManager");
		
		// Game Logic
		int idx = 0;
		foreach (Player player in GameManager.Players) {
			GD.Print($"Instantiating player {idx}...");
			InstantiatePlayer(idx, player);
			idx += 1;
		}
	}

	public override void _Process(double delta) {
	}  // Empty
	
	private void InstantiatePlayer(int idx, Player player) {
		BeingController being = _playerScene.Instantiate<BeingController>();
		being.Position = new Vector3(idx*2.5f, 0f, idx*2.5f);
		being.Name = player.Id.ToString();
		
		this.AddChild(being);
		being.Label.Text = player.Name;
	}
	//
	// private void Shooting() {
	// 	BulletController bullet = _bulletScene.Instantiate<BulletController>();
	// 	bullet.Position = Player.Position;
	// 	bullet.Rotation = Player.Rotation;
	// 	bullet.Speed = _bulletSpeed;
	// 	this.AddChild(bullet);
	// }
}